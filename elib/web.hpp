#ifndef ELIB_WEB_HPP
#define ELIB_WEB_HPP
# 
# include <elib/web/basic_web_error.hpp>
# include <elib/web/http.hpp>
# include <elib/web/socket.hpp>
# 
#endif /* ELIB_WEB_HPP */