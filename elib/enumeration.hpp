#ifndef ELIB_ENUMERATION_HPP
#define ELIB_ENUMERATION_HPP
# 
# include <elib/config.hpp>
# include <elib/enumeration/enum_helper.hpp>
# include <elib/enumeration/basic_enum_traits.hpp>
# include <elib/enumeration/enum_cast.hpp>
# include <elib/enumeration/enum_traits.hpp>
# include <elib/enumeration/enum_iterator.hpp>
# include <elib/enumeration/operators.hpp>
# 
#endif /* ELIB_ENUMERATION_HPP */