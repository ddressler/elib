#ifndef ELIB_CONFIG_HPP
#define ELIB_CONFIG_HPP
# 
# include <elib/config/platform_config.hpp>
# include <elib/config/compiler_config.hpp>
# include <elib/config/standard_config.hpp>
# include <elib/config/library_config.hpp>
# include <elib/config/workaround.hpp>
# 
# include <elib/config/coverity_scan_config.hpp>
# 
#endif /* ELIB_CONFIG_HPP */