#ifndef ELIB_EXCEPT_HPP
#define ELIB_EXCEPT_HPP
# 
# include <elib/except/fwd.hpp>
# include <elib/except/exception.hpp>
# include <elib/except/error_info.hpp>
# include <elib/except/throw_exception.hpp>
# include <elib/except/current_exception_cast.hpp>
# 
#endif /* ELIB_EXCEPT_HPP */