#ifndef ELIB_AUX_SUPPORT_HPP
#define ELIB_AUX_SUPPORT_HPP
#
# include <elib/aux/addressof.hpp>
# include <elib/aux/any_pod.hpp>
# include <elib/aux/anything.hpp>
# include <elib/aux/by_ref.hpp>
# include <elib/aux/by_val.hpp>
# include <elib/aux/common.hpp>
# include <elib/aux/declval.hpp>
# include <elib/aux/exchange.hpp>
# include <elib/aux/forward.hpp>
# include <elib/aux/move.hpp>
# include <elib/aux/static_const.hpp>
# include <elib/aux/swallow.hpp>
# 
#endif /* ELIB_AUX_SUPPORT_HPP */