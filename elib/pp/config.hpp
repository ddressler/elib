#ifndef ELIB_PP_CONFIG_HPP
#define ELIB_PP_CONFIG_HPP
# 
# include <elib/config.hpp>
# 
#
# define ELIB_PP_ARITHMETIC_LIMIT 256
# define ELIB_PP_ARITY_LIMIT 15
# 
#
#endif /* ELIB_PP_CONFIG_HPP */
