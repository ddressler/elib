#ifndef ELIB_LOG_HPP
#define ELIB_LOG_HPP
# 
# include <elib/log/config.hpp>
# include <elib/log/basic_log.hpp>
# include <elib/log/elog.hpp>
# include <elib/log/file_log.hpp>
# include <elib/log/log_level.hpp>
# include <elib/log/static_log.hpp>
# 
#endif /* ELIB_LOG_HPP */