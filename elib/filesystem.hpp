#ifndef ELIB_FILESYSTEM_HPP
#define ELIB_FILESYSTEM_HPP
# 
# include <elib/fs/config.hpp>
# include <elib/fs/filesystem_error.hpp>
# include <elib/fs/path.hpp>
# include <elib/fs/file_status.hpp>
# include <elib/fs/directory_iterator.hpp>
# include <elib/fs/operations.hpp>
# 
# endif                                               // ELIB_FILESYSTEM_HPP