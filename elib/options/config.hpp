#ifndef ELIB_OPTIONS_CONFIG_HPP
#define ELIB_OPTIONS_CONFIG_HPP

namespace elib { namespace options
{
    constexpr unsigned value_semantics_max_tokens = 15;
}}                                                          // namespace elib
#endif /* ELIB_OPTIONS_CONFIG_HPP */