#ifndef ELIB_RANGES_HPP
#define ELIB_RANGES_HPP
#
# include <elib/ranges/algorithm.hpp>
# include <elib/ranges/range.hpp>
# include <elib/ranges/numeric.hpp>
# include <elib/ranges/traits.hpp>
# 
#endif /* ELIB_RANGES_HPP */